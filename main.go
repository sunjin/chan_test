package main

import (
	"fmt"
)

func main() {
	fmt.Println("Learn Channel")

	ch := make(chan int, 100)

	stoppedchan := make(chan struct{}, 1)

	// gorutine 受信 chan
	go func(ch <-chan int, stoppedchan chan<- struct{}) {
		for {
			number, ok := <-ch
			if !ok {
				break
			}
			fmt.Println(number, "受信")
		}
		fmt.Println("受信gorutine終了")
		stoppedchan <- struct{}{}
	}(ch, stoppedchan)

	// gorutine 送信 chan
	go func(ch chan<- int) {
		number := 0
		for number < 1000 {
			ch <- number
			fmt.Println(number, "送信")
			number++
		}
		fmt.Println("送信gorutine終了")
		close(ch)
	}(ch)

	// 終了の通知が来るまで待機
	<-stoppedchan
	fmt.Println("終了")

}
